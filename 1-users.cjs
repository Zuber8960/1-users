const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interest: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/





//==============================================================================================

// Q1 Find all users who are interested in playing video games.

function usersPlayVideoGames(users) {
    users = Object.entries(users);
    let result = users.filter(user => {
        let  interests;
        if(user[1].interests){
            interests = user[1].interests
        }else if(user[1].interest){
            interests = user[1].interest;
        }
        const arr = interests.filter(interest => {
            return interest.includes("Video Games");
        })
        return arr.length;
    });

    result = Object.fromEntries(result);

    return result;


}

console.log(usersPlayVideoGames(users));


//==============================================================================================

// Q2 Find all users staying in Germany.

function stayingInGermany(users) {
    users = Object.entries(users);
    let result = users.filter(user => {
        // console.log()
        return user[1].nationality === "Germany";
    })
    result = Object.fromEntries(result);
    return result;
}


console.log(stayingInGermany(users));


//==============================================================================================

// Q3 Sort users based on their seniority level 
//    for Designation - Senior Developer > Developer > Intern
//    for Age - 20 > 10

function sortBasedOnSeniority(users) {
    users = Object.entries(users);
    let result = users.sort((user1, user2) => {
        const desgination1  = user1[1].desgination;
        const desgination2  = user2[1].desgination;
        const age1 = user1[1].age;
        const age2 = user2[1].age;
        if (desgination1.includes("Senior") && !desgination2.includes("Senior")) {
            return -1;
        } else if (desgination1.includes("Senior") && desgination2.includes("Senior")) {
            return age2 - age1;
        }else if ((!desgination1.includes("Senior") && desgination1.includes("Developer")) && (desgination2.includes("Intern"))) {
            return -1
        }  else if ((!desgination1.includes("Senior") && desgination1.includes("Developer")) && (!desgination2.includes("Senior")) && desgination2.includes("Developer")) {
            return age2 - age1;
        } else if (desgination1.includes("Intern") && desgination2.includes("Intern")) {
            return age2 - age1;
        };
    });
    result = Object.fromEntries(result);
    return result;

}

console.log(sortBasedOnSeniority(users));





//==============================================================================================

// Q4 Find all users with masters Degree.

function usersMasterDegree(users) {
    users = Object.entries(users);
    let result = users.filter(user => {
        return user[1].qualification = "Masters";
    });
    result = Object.fromEntries(result);
    return result;
}


console.log(usersMasterDegree(users));


//==============================================================================================

// Q5 Group users based on their Programming language mentioned in their designation.

function groupUsersBasedOnProgramming(users) {
    users = Object.entries(users);
    // const languages = ["Golang", "Javascript", "Python"];
    let result = users.reduce((obj, curr) => {
        let { desgination } = curr[1];
        let language;
        if (desgination.includes("Golang")) {
            language = "Golang";
        } else if (desgination.includes("Javascript")) {
            language = "Javascript";
        } else if (desgination.includes("Python")) {
            language = "Python";
        };

        if (obj[language] === undefined) {
            obj[language] = language;

            obj[language] = {};
            obj[language][curr[0]] = curr[1];
        } else {
            obj[language][curr[0]] = curr[1];

        };
        return obj;
    }, {});
    return result;
}

console.log(groupUsersBasedOnProgramming(users));




//==============================================================================================
